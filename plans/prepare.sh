#!/bin/bash

set -xe

# Install needed packages
dnf -y install /usr/bin/stap-prep
stap-prep

# Report installed packages
stap-report

# Set up SELinux so that it allows for userspace probing
setsebool allow_execmod on
setsebool allow_execstack on
setsebool deny_ptrace off

set +xe
